'use strict'
var jStat = require('jstat');

var utils = require('./utils');

exports.simulate_play_volados = (req, res, next) => {
    let dinero_inicial = Number(req.params.dinero);
    let dinero_apuesta = Number(req.params.apuesta);
    let dinero_meta = Number(req.params.meta);
    let corridas = Number(req.params.corridas);
    let i = 0;
    let cont_ganadas = 0;
    let cont_perdidas = 0;
    let cont_meta = 0;
    var table = [];
    while(i < corridas){
        let data = [];
        let gano = false;
        let meta = false;
        let random = jStat.uniform.sample(0, 1);
        data = {corrida: i, d_inicial: dinero_inicial, apuesta: dinero_apuesta, aleatorio: random, gano: gano, d_luego: dinero_inicial, meta: meta, cant_ganadas: cont_ganadas, cant_perdidias: cont_perdidas, cont_meta: cont_meta};
        if(random > 0.5){
            dinero_inicial -= dinero_apuesta;
            dinero_apuesta *= 2;
            cont_perdidas++;
            if(dinero_apuesta > dinero_inicial){
                dinero_apuesta = dinero_inicial;
            }
        }else{
            dinero_inicial += dinero_apuesta;
            dinero_apuesta = Number(req.params.apuesta);
            cont_ganadas++;
            gano = true;
        }
        data.d_luego = dinero_inicial;
        data.cant_perdidias = cont_perdidas;
        data.cant_ganadas = cont_ganadas;
        data.gano = gano;
        if(dinero_inicial <= 0){
            i++;
            dinero_inicial = Number(req.params.dinero);
            dinero_apuesta = Number(req.params.apuesta);
        }else if(dinero_inicial >= dinero_meta){
            i++;
            meta = true;
            cont_meta++;
            dinero_inicial = Number(req.params.dinero);
            dinero_apuesta = Number(req.params.apuesta);
        }
        data.corrida = i;
        data.meta = meta;
        data.cont_meta = cont_meta;
        table.push(data);
    }
    res.json(table);
}
