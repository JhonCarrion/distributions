'use strict'
var jStat = require('jstat');

var utils = require('./utils');


exports.calculate_binomial_distribution_pdf = (req, res, next) => {
    let tam = Number(req.params.tam); 
    var normData = jStat.seq(0, 1, tam, (x) => {
        return jStat.binomial.pdf(x, tam, Number(req.params.p));
    });
    res.json({dist: normData, 
              mean: utils.mean(normData), 
              std: utils.std_poblation(normData), 
              median: utils.median(normData), 
              variance: utils.variance_poblation(normData)});
}

