'use strict'
var jStat = require('jstat');

var utils = require('./utils');

exports.calculate_uniform_distribution_pdf = (req, res, next) => {
    let tam = Number(req.params.tam); 
    //    var normData = jStat.seq(0, 1, tam, (x) => {
    //        return jStat.uniform.pdf(x, Number(req.params.a), Number(req.params.b));
    //    });
    var list = utils.random_uniform_vector(0, 1, tam);
    var normData = [];
    list.forEach(function(item){
        normData.push(jStat.uniform.pdf(item, Number(req.params.a), Number(req.params.b)));
    });
    res.json({dist: normData, 
              mean: jStat.uniform.mean(Number(req.params.a), Number(req.params.b)), 
              std: Math.sqrt(jStat.uniform.variance(Number(req.params.a), Number(req.params.b))), 
              median: utils.median(normData), 
              variance: jStat.uniform.variance(Number(req.params.a), Number(req.params.b))});
}
