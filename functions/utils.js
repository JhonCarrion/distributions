'use strict'
var jStat = require('jstat');

exports.mean = (vector) => {
    return jStat.mean(vector);
}

exports.median = (vector) => {
    return jStat.median(vector);
}

exports.min = (vector) => {
    return jStat.min(vector);
}

exports.max = (vector) => {
    return jStat.max(vector);
}

exports.mode = (vector) => {
    return jStat.mode(vector);
}

exports.variance_poblation = (vector) => {
    return jStat.variance(vector);
}

exports.variance_instead = (vector) => {
    return jStat.variance(vector, true);
}

exports.std_poblation = (vector) => {
    return jStat.stdev(vector);
}

exports.std_instead = (vector) => {
    return jStat.stdev(vector, true);
}

exports.random_uniform_vector = (min, max, tam) => {
    let randomArray = [];
    for(let i = 0; i < tam; i++){
        randomArray.push(jStat.uniform.sample(min, max));
    }
    return randomArray;
}

/**
 * @api {get} /random/:a/:b/:tam Delivers a random number matrix that follows the normal distribution, requesting the minimum and maximum value of the interval.
 * @apiName DistributionNormal
 * @apiGroup Distributions
 *
 * @apiParam {Number} a Minimum value of the interval.
 * @apiParam {Number} b Maximum value of the interval.
 * @apiParam {Number} tam How many numbers do you want?
 *
 * @apiSuccess {JSON} Array of random numbers.
 */
exports.generate_random_array_min_max = (req, res, next) => {
    let randomArray = [];
    let tam = Number(req.params.tam); 
    for(let i = 0; i < tam; i++){
        randomArray.push(jStat.randn(Number(req.params.a), Number(req.params.b)));
    }
    return randomArray;
}

/**
 * @api {get} /random/one/:a/:b Returns a random number that follows the normal distribution, requesting the minimum and maximum value of the interval.
 * @apiName DistributionNormal
 * @apiGroup Distributions
 *
 * @apiParam {Number} a Minimum value of the interval.
 * @apiParam {Number} b Maximum value of the interval.
 *
 * @apiSuccess {Number} random number.
 */
exports.generate_random_min_max = (req, res, next) => {
    return jStat.randn(Number(req.params.a), Number(req.params.b));
}