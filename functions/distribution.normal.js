'use strict'
var jStat = require('jstat');

var utils = require('./utils');

/**
 * @api {get} /normal/random/:mean/:std/:tam Delivers a matrix of random numbers that follow the normal distribution, requesting, the mean and the standard deviation.
 * @apiName DistributionNormal
 * @apiGroup Distributions
 *
 * @apiParam {Number} mean Value of the average.
 * @apiParam {Number} std Value of the standard deviation.
 * @apiParam {Number} tam How many numbers do you want?
 *
 * @apiSuccess {JSON} Array of random numbers.
 */
exports.generate_random_array_mean_std = (req, res, next) => {
    let randomArray = [];
    let tam = Number(req.params.tam); 
    for(let i = 0; i < tam; i++){
        randomArray.push(jStat.normal.sample(Number(req.params.mean), Number(req.params.std)));
    }
    return randomArray;
}

/**
 * @api {get} /normal/random/:mean/:std Delivers a random number that follows the normal distribution, the data request is: the mean and the standard deviation.
 * @apiName DistributionNormal
 * @apiGroup Distributions
 *
 * @apiParam {Number} mean Value of the average.
 * @apiParam {Number} std Value of the standard deviation.
 *
 * @apiSuccess {Number} random number.
 */
exports.generate_random_mean_std = (req, res, next) => {
    return jStat.normal.sample(Number(req.params.mean), Number(req.params.std));
}

exports.calculate_normal_distribution_pdf = (req, res, next) => {
    return jStat.normal.pdf(jStat.uniform.sample(0, 1), Number(req.params.mean), Number(req.params.std));
}
//cdf cumulative distribution function
//pdf probability density function
//inv inverse of cumulative distribution function
exports.calculate_normal_distribution_pdf_auto = (req, res, next) => {
    let tam = Number(req.params.tam); 
    let data = utils.random_uniform_vector(0, 1, tam);
    let mean = utils.mean(data);
    let stdev = utils.std_poblation(data);
    let min = utils.min(data);
    let max = utils.max(data);

    var normData = jStat.seq(min, max, tam, (x) => {
        return jStat.normal.pdf(x, mean, stdev);
    });
    res.json({dist: normData, mean: mean, std: stdev, median: utils.median(data), variance: utils.variance_poblation(data)});
}

exports.calculate_normal_distribution_pdf = (req, res, next) => {
    let tam = Number(req.params.tam); 
    var normData = jStat.seq(0, 1, tam, (x) => {
        return jStat.normal.pdf(x, Number(req.params.mean), Number(req.params.std));
    });
    res.json({dist: normData, 
              mean: jStat.normal.mean(Number(req.params.mean), Number(req.params.std)), 
              std: Math.sqrt(jStat.normal.variance(Number(req.params.mean), Number(req.params.std))), 
              median: jStat.normal.median(Number(req.params.mean), Number(req.params.std)), 
              variance: jStat.normal.variance(Number(req.params.mean), Number(req.params.std))});
}

