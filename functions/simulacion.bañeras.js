'use strict'
var jStat = require('jstat');

var utils = require('./utils');

exports.simulate_tinas = (req, res, next) => {
    let corridas = Number(req.params.corridas);
    let nro_tinas = Number(req.params.tinas);
    let i = 0;
    var table = [];
    while(i < corridas){
        let x = 0;
        let data = [];
        let tina = 0;
        let x_acum = 0;
        for(let j = 0; j < nro_tinas; j++){
            tina++;
            let random = jStat.uniform.sample(0, 1);
            if(random < 0.5){
                x = eq_1(random);
            }else{
                x = eq_2(random);
            }
            x_acum += x;
            if(x_acum > 1000){
                data = {corrida: i, tina: tina, random: random, peso: x, peso_acum: x_acum, exedio: true};
            }else{
                data = {corrida: i, tina: tina, random: random, peso: x, peso_acum: x_acum, exedio: false};
            }
            table.push(data);
        }
        i++;
    }
    res.json(table);
}

var eq_1 = (ri) => {
    return (190 + Math.sqrt(800 * ri));
}
var eq_2 = (ri) => {
    return (230 - Math.sqrt(800 * (1 - ri)));
}
