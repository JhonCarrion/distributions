SISTEMA DE GENERACIÓN DE DISTRIBUCIONES DE PROBABILIDAD
=============================
![Sello UNL](https://upload.wikimedia.org/wikipedia/commons/d/df/UNL3.png) 


Proyecto desarrollado por Jhon Alexander Carrión Piedra estudiante de la carrera de Ingeniería en Sistemas de la [Universidad Nacional de Loja](http://www.unl.edu.ec), orientado a la generacion de graficas y datos estadisticos de las diferentes distribuciones de probabilidad.


Las distribuciones que se ofrecen son:
  - Distribución Normal
  - Distribución Normal, sin ennviar media y desviación estandar
  - Distribución Binomial
  - Distribución Uniforme

Instrucciones:
=============================

	
Para instalar Node.js desde NVM
--------------------------------------------------------------
Instalar los siguientes paquetes de forma global

    curl https://raw.githubusercontent.com/creationix/nvm/v0.13.1/install.sh | bash
    source ~/.bash_profile
    nvm list-remote
    nvm install v11.9.0
    nvm use v11.9.0
    nvm alias default v11.9.0
    node -v
    
Otros paquetes necesarios

	$ npm install express -g
	$ npm install express-generator -g
	$ npm install pm2 -g

    
Para subir el proyecto al servidor
--------------------------------------------------------------

Clonar el repositorio

	$ git clone https://gitlab.com/JhonCarrion/distributions.git

Ingresar a la carpeta

	$ cd distributions

Instalar las dependencias de Node.js

	$ npm install

Si se desea realizar pruebas se pude utilizar *nodemon*

	$ npm install nodemon -g
	$ nodemon

Para mantener el servicio en producción se puede utilizar *pm2*

	$ pm2 start bin/www --name "nombre_del_proyecto"

Para ver los *Log*

	$ pm2 log

Para reiniciar el pm2

	$ pm2 restart "nombre_del_proyecto"

Para determinar el proceso del pm2

	$ pm2 stop "nombre_del_proyecto"

Para mas información del *pm2* visitar la [Página oficial de pm2](http://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/)

Probar Sistema de forma local
--------------------------------------------------------------

Ya una vez instalado NodeJS se instala junto con el, el gestor de paquetes NPM; este servira para instalar los paquetes necesarios.
- Se debe instalar de forma global el paquete nodemon (npm install -g nodemon)
- Luego se puede ejecutar el sistema estando dentro de la carpeta solo escribiendo nodemon
- Ahora el sistema puede ser visualizado en el navegador, en la dirección http://localhost:3000