var express = require('express');
var router = express.Router();

var normal = require('../functions/distribution.normal');
var binomial = require('../functions/distribution.binomial');
var uniform = require('../functions/distribution.uniform');
var volados = require('../functions/simulacion.volados');
var tinas = require('../functions/simulacion.bañeras');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Distribuciones', fragment: "normal"});
});
/* GET volados page. */
router.get('/juego', function(req, res, next) {
    res.render('index', { title: 'Volados', fragment: "volados"});
});
/* GET tinas page. */
router.get('/tinas', function(req, res, next) {
    res.render('index', { title: 'Bañeras', fragment: "tinas"});
});

router.get('/normal/:mean/:std/:tam', normal.calculate_normal_distribution_pdf);
router.get('/normal/:tam', normal.calculate_normal_distribution_pdf_auto);
router.get('/binomial/:p/:tam', binomial.calculate_binomial_distribution_pdf);
router.get('/uniform/:a/:b/:tam', uniform.calculate_uniform_distribution_pdf);
router.get('/volados/:dinero/:apuesta/:meta/:corridas', volados.simulate_play_volados);
router.get('/tinas/:tinas/:corridas', tinas.simulate_tinas);

module.exports = router;
